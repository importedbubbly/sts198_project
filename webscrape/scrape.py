import requests
from bs4 import BeautifulSoup

client = requests.Session()

HOMEPAGE_URL = 'https://www.linkedin.com'
LOGIN_URL = 'https://www.linkedin.com/uas/login-submit'

html = client.get(HOMEPAGE_URL).content
soup = BeautifulSoup(html, 'html.parser')

csrf = soup.find(id="loginCsrfParam-login")['value']

login_information = {
    'session_key':'Login',
    'session_password':'Password',
    'loginCsrfParam': csrf,
}

client.post(LOGIN_URL, data=login_information)

r = client.get('https://www.linkedin.com/job/jobs-in-san-francisco-ca/?sort=date&trk=jserp_sort_date', headers={"User-Agent": "Requests"})

# print r.content

bs = BeautifulSoup(r.text, "html.parser")


# print bs.text

jobs = soup.find_all("a")
for links in bs.find_all('a'):
    print (links.get('href'))



### USING URLLIB2 GIVES FORBIDDEN ERROR. NEED TO 'LOGIN' AS A BROWSER TO ACCESS CONTENT ###
# import urllib2
# file = urllib2.urlopen("https://www.linkedin.com/job/jobs-in-san-francisco-ca/")
# html = file.read()
# file.close()

# soup = BeautifulSoup(html, "html.parser")
# print soup.title.string
# for links in soup.find_all('a'):
#     print (links.get('href'))


# install.packages("XML")
# install.packages("RCurl")
library(XML)
library(RCurl)

setwd("/Users/Sharon/Desktop/pages")

# creating url
site = "https://www.linkedin.com"
c.beforeCompany = "/company/"
c.afterCompany = "?trk=jserp_company_name"

# here, add the company names you are interested in.
# make sure the company names exist!
companyNameList = c("nelson-technology","apple","ernstandyoung")

# Lines 17-25 grabs the HTML for that company page
company.num = 1
while(company.num <= length(companyNameList)){
  # combine these into the company webpage
  c.site = paste(site, c.beforeCompany, companyNameList[company.num], c.afterCompany, sep = "")
  html.company = getURLContent(c.site)
  saveRDS(html.company, companyNameList[company.num])
  company.num = company.num + 1
}

# Then, create a new data frame for company
company = data.frame(Company = NA, Industry = NA, numEmployees = NA, Type = NA, yearFounded = NA, numFollowers = NA, Website = NA, headStreet = NA, headCity = NA, headState = NA, headZip = NA)
# Company refers the name of the Company
# Industry refers to the Industry that the Company is in
# numEmployees refers to the different groups of how many employees the company has
# Type refers to the type of company, such as private or public
# yearFounded refers to the year that the company was founded
# numFollowers refers to the number of followers of the company
# website refers to the company's website
# headStreet refers to the street that the Headquarters of the company is located at
# headCity refers to the city that the Headquarters of the company is located at
# headState refers to the state that the Headquarters of the company is located at
# headZip refers to the zip code that the Headquarters of the company is located at

# Finally Line 46 reads through the HTML to extract tags that we are interested in. 
# Lines 43 - 70 extracts information we are interested in, and puts it into the new data frame
company.num = 1
while(company.num <= length(companyNameList)){
  html.company = readRDS(companyNameList[company.num])
  html.company = htmlParse(html.company, asText = TRUE)
  
  company = rbind(company, rep(NA, ncol(company)))

  # This is for the top bar
  companyTopBar = getNodeSet(html.company, "//div[@class = 'top-bar with-wide-image with-nav big-logo']")
  
  company$Company[company.num] = gsub("\n", "", xmlValue(xpathApply(companyTopBar[[1]], ".//span[@itemprop = 'name']")[[1]]))
  company$numEmployees[company.num] = gsub("\n", "", xmlValue(xpathApply(companyTopBar[[1]], ".//p[@class = 'company-size']")[[1]]))
  company$numFollowers[company.num] = gsub("\n", "", xmlValue(xpathApply(companyTopBar[[1]], ".//p[@class = 'followers-count']/strong")[[1]]))
  
  # This is for the main information section
  companyMain = getNodeSet(html.company, "//div[@id = 'content']")
  
  company$Industry[company.num] = gsub("\n", "", xmlValue(xpathApply(companyMain[[1]], ".//li[@class = 'industry']/p")[[1]]))
  company$Type[company.num] = gsub("\n", "", xmlValue(xpathApply(companyMain[[1]], ".//li[@class = 'type']/p")[[1]]))
  company$yearFounded[company.num] = gsub("\n", "", xmlValue(xpathApply(companyMain[[1]], ".//li[@class = 'founded']/p")[[1]]))
  company$Website[company.num] = gsub("\n", "", xmlValue(xpathApply(companyMain[[1]], ".//li[@class = 'website']/p")[[1]]))
  company$headStreet[company.num] = gsub("\n", "", xmlValue(xpathApply(companyMain[[1]], ".//li[@class = 'vcard hq']//span[@class = 'street-address']")[[1]]))
  company$headCity[company.num] = gsub("\n", "", xmlValue(xpathApply(companyMain[[1]], ".//li[@class = 'vcard hq']//span[@class = 'locality']")[[1]]))
  company$headState[company.num] = gsub("\n", "", xmlValue(xpathApply(companyMain[[1]], ".//li[@class = 'vcard hq']//abbr[@class = 'region']")[[1]]))
  company$headZip[company.num] = gsub("\n", "", xmlValue(xpathApply(companyMain[[1]], ".//li[@class = 'vcard hq']//span[@class = 'country-name']")[[1]]))
  
  company.num = company.num + 1
}
# remove last row, filled with NA's
company = company[-nrow(company),]


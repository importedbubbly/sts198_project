setwd("~/Desktop/sts198/project/finalUnstem")

dictcount = readRDS("countFinal")
jobPosting = readRDS("jpostFinal")
jobDesc = paste(jobPosting$jobDescription, jobPosting$desSkillAndExp)
jobPosting = jobPosting[which(duplicated(jobDesc) == FALSE), ]
jobDesc = paste(jobPosting$jobDescription, jobPosting$desSkillAndExp)
jobDescOrig = jobDesc
#####

#####
# Normalizing the job descriptions and dictionary
#####
stopWords = c(stopwords(kind = "en"), "", "s", "re", "ll", "ve", "m", "d", "t", "us", "com", "www") 
'%nin%' <- Negate('%in%')

# This line gets rid of all below symbols except for + # and /
# '[]\\?!\"\'#$%&(){}�+*/:;,._`|~\\[<=>@\\^-]'
jobDesc = lapply(jobDesc, function(t) {
  t = tolower(t)
  t = gsub("[^[:alnum:]+# ]", " ", t)
  t = gsub("\\+", "P", t)
  t = gsub("#", "H", t)
  t = unlist(strsplit(t, " "))
  t = removeNumbers(t)
  t = t[t %nin% stopWords]
  #t = stemDocument(t)
  t = paste(t, collapse = " ")
  t = sprintf(" %s ", t)
  return(t)
})
jobDesc = unlist(jobDesc)

#### run dictBowProcessing.R Ln 1-74 to get jobDesc (standardized text) ####
jobInd <- which( grepl(" computer science ", jobDesc) )
length(jobInd)
hasMath = jobDesc[jobInd]
hasMathFull = jobDesc[jobInd]
hasMathDF = jobPosting[jobInd,]

# create new jobDesc + desSkill variable, jobDescSubset in our subset
# jobDescSubset = paste(hasMath$jobDescription, hasMath$desSkillAndExp)


# standardize dictionary
stopWords = c(stopwords(kind = "en"), "", "s", "re", "ll", "ve", "m", "d", "t", "us", "com", "www") 
'%nin%' <- Negate('%in%')

setwd("~/Desktop/sts198/project")
dict = read.csv("job_skills_dict.csv")
dict.orig = dict

dict = as.character(dict$skills)
dict.order = order(nchar(dict), decreasing = TRUE)
dict = dict[dict.order]

dict = lapply(dict, function(t) {
  t = tolower(t)
  t = unlist(strsplit(t, " "))
  t = removeNumbers(t)
  t = t[t %nin% stopWords]
  t = gsub("[^[:alnum:]+# ]", " ", t)
  t = gsub("\\+", "P", t)
  t = gsub("#", "H", t)
  #   t = stemDocument(t)
  t = paste(t, collapse = " ")
  t = sprintf(" %s ", t)
  return(t)
})
#dict.order = order(nchar(dict), decreasing = TRUE)
dict = unlist(dict)
dict.count = data.frame(term = dict, count = 0, totalCount = 0, row.names = NULL)


# compare jobDescSubset with our dictionary to find frequency count
dictNum = 1
jobDescNum = 1
counter = 1

# The first loop gets us the job description that we need
while(jobDescNum <= length(hasMath)){
  #while(jobDescNum <= 50){
  dictNum = 1
  # The second loop rolls through the dictionary and removes each phrase from the job description and then increments the count up 1
  while(dictNum <= length(dict)){
    
    # This if statement checks if the term exists inside of the job description
    if(grepl(dict[dictNum], hasMath[jobDescNum], fixed = TRUE) == TRUE){
      dict.count[dictNum, ]$count = dict.count[dictNum, ]$count + 1
      hasMath[jobDescNum] = gsub(dict[dictNum], " ", hasMath[jobDescNum])
    } # If statement loop end
    
    # This if statement checks the original job description rather than the edited one
    if(grepl(dict[dictNum], hasMathFull[jobDescNum], fixed = TRUE) == TRUE){
      dict.count[dictNum, ]$totalCount = dict.count[dictNum, ]$totalCount + 1
    }
    
    dictNum = dictNum + 1
    
  } # Second loop end
  
  jobDescNum = jobDescNum + 1
  cat(paste(counter, " "))
  counter = counter + 1
  
} # Third loop end

dict.count = dict.count[order(dict.count$totalCount, decreasing = TRUE), ]
dict.count = subset(dict.count, count > 15)
  test = subset(dict.count, count > 15)
View(test)
View(dict.count)

jobDescBoolM = matrix(0, length(hasMath), nrow(dict.count), dimname = list(hasMathDF$jobID, dict.count$term))


# Resetting our job descriptions
hasMath = hasMathFull

#### creating bool matrix #####

dictNum = 1
jobDescNum = 1
counter = 1

while(jobDescNum <= length(hasMath)){
  #while(jobDescNum <= 50){
  dictNum = 1
  # The second loop rolls through the dictionary and removes each phrase from the job description and then increments the count up 1
  while(dictNum <= length(colnames(jobDescBoolM))){
    
    # This if statement checks if the term exists inside of the job description
    if(grepl(dict.count$term[dictNum], hasMath[jobDescNum], fixed = TRUE) == TRUE){
      jobDescBoolM[jobDescNum, dictNum] = 1
      hasMath[jobDescNum] = gsub(dict.count$term[dictNum], " ", hasMath[jobDescNum])
    } # If statement loop end
    
    dictNum = dictNum + 1
    
  } # Second loop end
  
  jobDescNum = jobDescNum + 1
  cat(paste(counter, " "))
  counter = counter + 1
  
} # Third loop end
#####

distM <- as.matrix(dist(t(jobDescBoolM),"euclidean"))

#### LOOK AT DISTM, SET THRESHOLD TO ONLY SHOW EDGES WITH MIN DIST ####
# source("http://www.bioconductor.org/biocLite.R")
# biocLite("Ruuid")
# biocLite("graph")
# biocLite("Rgraphviz")
library(Rgraphviz)

#### THRESHOLDING, ONLY CARE ABOUT DISTANCES LESS THAN 4 ####
distM[distM > 5] = 0

am.graph<-new("graphAM", adjMat=distM, edgemode="directed")
plot(am.graph, attrs = list(node = list(fillcolor = "lightblue"),
                              edge = list(arrowsize=0.5)),
     main="Skills Associated with Computer Science")


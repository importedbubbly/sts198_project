setwd("~/Desktop/sts198/project/finalUnstem")

##### LOAD DATA #####
jobDescBoolM <- readRDS("boolFinal")
cl <- readRDS("clusterFinal")
dictcount <- readRDS("countFinal")
distM <- readRDS("distFinal")
j <- readRDS("jpostFinal")

#### SPLIT DATA FRAME BY CLUSTER ####
# Splitting jobPostings and BoolM into their clusters
clusterMatrix = split(data.frame(jobDescBoolM), cl$cluster)

# This is the code for finding the sum of each term for each cluster
compareMatrix = sapply(clusterMatrix, function(cluster){
  sumOfTerms = vector(length = ncol(cluster))
  for(i in 1:ncol(cluster)){
    sumOfTerms[i] = sum(cluster[,i])
  }
  return(sumOfTerms)
})
compareMatrix = t(compareMatrix)
colnames(compareMatrix) = colnames(jobDescBoolM)

##### DEFINE TOP SKILLS PER CLUSTER #####

clusterTopTenTerms = list()
for(i in 1:length(rownames(compareMatrix))){
  clusterTopTenTerms[[i]] = head(sort(compareMatrix[i,], decreasing = TRUE))
}

###### PLOT ######

library(reshape2)
library(ggplot2)

tt = melt(clusterTopTenTerms)
tt$words = names(unlist(clusterTopTenTerms))
# tt = tt[order(tt$words),]

theme_set(theme_gray(base_size = 20))                 # make text bigger
g = ggplot(data=tt, aes(x=value, y=words, group=L1, 
                        colour=as.factor(L1))) +
  xlab("Count") +
  ylab("Skill Words") +
  ggtitle("Popularity of Skill Words by Cluster") +
  geom_point(size = 8, alpha = 0.8)                   # set larger pt size and transparency
g + scale_color_discrete(name="Cluster")              # rename legend title
